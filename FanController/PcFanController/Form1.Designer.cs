﻿namespace PcFanController
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.SysMonitorGroupBox = new System.Windows.Forms.GroupBox();
            this.AverageCPUInfoLabel = new System.Windows.Forms.Label();
            this.CoreTempConnectionLabel = new System.Windows.Forms.Label();
            this.SerialPortCombo = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SerialPortConnectBnt = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.SysMonitorGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "CPU name";
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "label3";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "label4";
            this.label4.Click += new System.EventHandler(this.label3_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "label5";
            this.label5.Click += new System.EventHandler(this.label3_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "label6";
            this.label6.Click += new System.EventHandler(this.label3_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "label7";
            this.label7.Click += new System.EventHandler(this.label3_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "label8";
            this.label8.Click += new System.EventHandler(this.label3_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 137);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "label9";
            this.label9.Click += new System.EventHandler(this.label3_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 230);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Connecting to Arduino...";
            this.label10.Click += new System.EventHandler(this.label3_Click);
            // 
            // SysMonitorGroupBox
            // 
            this.SysMonitorGroupBox.Controls.Add(this.AverageCPUInfoLabel);
            this.SysMonitorGroupBox.Controls.Add(this.label1);
            this.SysMonitorGroupBox.Controls.Add(this.label2);
            this.SysMonitorGroupBox.Controls.Add(this.label9);
            this.SysMonitorGroupBox.Controls.Add(this.label3);
            this.SysMonitorGroupBox.Controls.Add(this.label8);
            this.SysMonitorGroupBox.Controls.Add(this.label4);
            this.SysMonitorGroupBox.Controls.Add(this.label6);
            this.SysMonitorGroupBox.Controls.Add(this.label7);
            this.SysMonitorGroupBox.Controls.Add(this.label5);
            this.SysMonitorGroupBox.Location = new System.Drawing.Point(12, 36);
            this.SysMonitorGroupBox.Name = "SysMonitorGroupBox";
            this.SysMonitorGroupBox.Size = new System.Drawing.Size(219, 178);
            this.SysMonitorGroupBox.TabIndex = 2;
            this.SysMonitorGroupBox.TabStop = false;
            this.SysMonitorGroupBox.Text = "System monitor";
            // 
            // AverageCPUInfoLabel
            // 
            this.AverageCPUInfoLabel.AutoSize = true;
            this.AverageCPUInfoLabel.Location = new System.Drawing.Point(6, 162);
            this.AverageCPUInfoLabel.Name = "AverageCPUInfoLabel";
            this.AverageCPUInfoLabel.Size = new System.Drawing.Size(41, 13);
            this.AverageCPUInfoLabel.TabIndex = 2;
            this.AverageCPUInfoLabel.Text = "label12";
            // 
            // CoreTempConnectionLabel
            // 
            this.CoreTempConnectionLabel.AutoSize = true;
            this.CoreTempConnectionLabel.Location = new System.Drawing.Point(12, 13);
            this.CoreTempConnectionLabel.Name = "CoreTempConnectionLabel";
            this.CoreTempConnectionLabel.Size = new System.Drawing.Size(136, 13);
            this.CoreTempConnectionLabel.TabIndex = 3;
            this.CoreTempConnectionLabel.Text = "CoreTempConnectionLabel";
            // 
            // SerialPortCombo
            // 
            this.SerialPortCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SerialPortCombo.FormattingEnabled = true;
            this.SerialPortCombo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SerialPortCombo.Location = new System.Drawing.Point(6, 36);
            this.SerialPortCombo.Name = "SerialPortCombo";
            this.SerialPortCombo.Size = new System.Drawing.Size(121, 21);
            this.SerialPortCombo.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SerialPortConnectBnt);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.SerialPortCombo);
            this.groupBox1.Location = new System.Drawing.Point(12, 256);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(219, 100);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // SerialPortConnectBnt
            // 
            this.SerialPortConnectBnt.Location = new System.Drawing.Point(133, 36);
            this.SerialPortConnectBnt.Name = "SerialPortConnectBnt";
            this.SerialPortConnectBnt.Size = new System.Drawing.Size(75, 23);
            this.SerialPortConnectBnt.TabIndex = 6;
            this.SerialPortConnectBnt.Text = "Connect";
            this.SerialPortConnectBnt.UseVisualStyleBackColor = true;
            this.SerialPortConnectBnt.Click += new System.EventHandler(this.SerialPortConnectBnt_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Set serial port:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(246, 367);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CoreTempConnectionLabel);
            this.Controls.Add(this.SysMonitorGroupBox);
            this.Controls.Add(this.label10);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Mauxx\'s Fan Controller";
            this.SysMonitorGroupBox.ResumeLayout(false);
            this.SysMonitorGroupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox SysMonitorGroupBox;
        private System.Windows.Forms.Label CoreTempConnectionLabel;
        private System.Windows.Forms.ComboBox SerialPortCombo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button SerialPortConnectBnt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label AverageCPUInfoLabel;
    }
}

