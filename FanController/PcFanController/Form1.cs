﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Timers;
using GetCoreTempInfoNET;

using System.IO.Ports;

namespace PcFanController
{
    public partial class Form1 : Form
    {
        //CoreTemp connection object
        CoreTempInfo CTInfo;

        //Serial port for arduino communications
        private SerialPort ArduinoPort = new SerialPort();

        //SystemMonitor
        Label[] CpuInfoLabels;
        String DelfaultSerialPort = "";

        public Form1()
        {
            InitializeComponent();
            this.FormClosed += new FormClosedEventHandler(f_FormClosed);
            Init();
        }

        private void Init()
        {
            //System info visualization init

            //Add cpu labels
            CpuInfoLabels = new Label[] { label2, label3, label4, label5, label6, label7, label8, label9 };

            //Initiate CoreTempInfo class.
            CTInfo = new CoreTempInfo();

            CoreTempConnectionLabel.Text = "Connecting to CoreTemp to get system data...";
            
            //Arduino connection initialization
            ArduinoPort.Parity = Parity.None;
            ArduinoPort.StopBits = StopBits.One;
            ArduinoPort.DataBits = 8;
            ArduinoPort.Handshake = Handshake.None;
            ArduinoPort.RtsEnable = true;
            ArduinoPort.BaudRate = 9600;

            DelfaultSerialPort = "COM5";//LoadDefaultPort();

            //First time get info manually and then start timer
            RefreshInfo_Elapsed(null, null);
            timer1.Start();
        }

        void RefreshInfo_Elapsed(object sender, ElapsedEventArgs e)
        {
            //Attempt to read shared memory.
            bool bReadSuccess = CTInfo.GetData();

            //If read was successful the post the new info on the console.
            if (bReadSuccess)
            {
                SysMonitorGroupBox.Enabled = true;
                CoreTempConnectionLabel.Text = "Successfully connected to CoreTemp.";

                label1.Text = CTInfo.GetCPUName;
                SetCPULabelDisabledText();

                uint cpuAverageTempC = 0;
                uint cpuAverageLoad = 0;

                uint index = 0;
                /*Console.WriteLine("CPU Name: " + CTInfo.GetCPUName);
                Console.WriteLine("CPU Speed: " + CTInfo.GetCPUSpeed + "MHz (" + CTInfo.GetFSBSpeed + " x " + CTInfo.GetMultiplier + ")");
                Console.WriteLine("CPU VID: " + CTInfo.GetVID + "v");
                Console.WriteLine("Physical CPUs: " + CTInfo.GetCPUCount);
                Console.WriteLine("Cores per CPU: " + CTInfo.GetCoreCount);*/
                for (uint i = 0; i < CTInfo.GetCPUCount; i++)
                {
                   // //Console.WriteLine("CPU #{0}", i);
                   // Console.WriteLine("Tj.Max: " + CTInfo.GetTjMax[i] + "°" + TempType);
                    for (uint g = 0; g < CTInfo.GetCoreCount; g++)
                    {
                        index = g + (i * CTInfo.GetCoreCount);

                        if(index < CpuInfoLabels.Length)
                        {
                            float tempC = CTInfo.IsFahrenheit ? (CTInfo.GetTemp[index] - 32) * 5 / 9 : CTInfo.GetTemp[index];
                            float tjMaxC = CTInfo.IsFahrenheit ? (CTInfo.GetTjMax[index] - 32) * 5 / 9 : CTInfo.GetTjMax[index];
                            uint coreLoad = CTInfo.GetCoreLoad[index];

                            tempC = CTInfo.IsDistanceToTjMax ? tjMaxC - tempC : tempC;

                            CpuInfoLabels[index].Text = "CPU #" + index + " temp: " + tempC + "°C - load: " + coreLoad + "%";

                            cpuAverageTempC += (uint)tempC;
                            cpuAverageLoad += coreLoad;

                            /*if (CTInfo.IsDistanceToTjMax)
                                Console.WriteLine("Core #{0}: {1}°{2} to TjMax, {3}% Load", index, CTInfo.GetTemp[index], TempType, CTInfo.GetCoreLoad[index]);
                            else
                                Console.WriteLine("Core #{0}: {1}°{2}, {3}% Load", index, CTInfo.GetTemp[index], TempType, CTInfo.GetCoreLoad[index]);*/
                        }
                    }
                }

                //Show average data
                cpuAverageTempC /= (index + 1);
                cpuAverageLoad /= (index + 1);
                AverageCPUInfoLabel.Text = "Average CPU: " + cpuAverageTempC + "°C - load: " + cpuAverageLoad + "%";

                //Update serial ports
                string[] ports = SerialPort.GetPortNames();
                foreach (string port in ports)
                {
                    if (SerialPortCombo.Items.Contains(port) == false)
                        SerialPortCombo.Items.Add(port);
                    
                    if(DelfaultSerialPort == port)
                    {
                        SerialPortCombo.Text = port;
                    }
                }

                //Sending data to Arduino
                if (ArduinoPort.IsOpen)
                {
                    ArduinoPort.Write("c"+cpuAverageTempC + "*" + cpuAverageLoad +"%");
                    Console.WriteLine("Serial port OPENED sending data.:");
                }
                else
                {
                    Console.WriteLine("Serial port CLOSED not sending data.");

                    //Try to automatically reconnect
                    ConnectToSerialPort();
                }

            }
            else
            {
                SysMonitorGroupBox.Enabled = false;
                CoreTempConnectionLabel.Text = "Connection to CoreTemp failed, reconnecting...";
                SetCPULabelDisabledText();

                //Startup of CoreTemp
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "CoreTemp.exe";
                //startInfo.Arguments = "/C insert command arguments here, /C is always needed to forward commands";
                process.StartInfo = startInfo;
                process.Start();

                Console.WriteLine();
                Console.WriteLine("Internal error name: " + CTInfo.GetLastError);
                Console.WriteLine("Internal error value: " + (int)CTInfo.GetLastError);
                Console.WriteLine("Internal error message: " + CTInfo.GetErrorMessage(CTInfo.GetLastError));
            }
        }

        private void SetCPULabelDisabledText()
        {
            for (uint index = 0; index < CpuInfoLabels.Length; ++index)
            {
                CpuInfoLabels[index].Text = "CPU #" + index + " no data";
            }
            AverageCPUInfoLabel.Text = "Average CPU: no data";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            RefreshInfo_Elapsed(null, null);
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        void f_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                //ArduinoPort.Write("DIS*");
                ArduinoPort.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SaveDefaultPort(String newDefaultPort)
        {
            System.IO.File.WriteAllText("SerialPort.config", newDefaultPort);
        }

        private String LoadDefaultPort()
        {
            try
            {
                return System.IO.File.ReadAllText("SerialPort.config");
            }
            catch(Exception e)
            {
                return "";
            }
        }

        private void SerialPortConnectBnt_Click(object sender, EventArgs e)
        {
            ConnectToSerialPort();
        }

        private void ConnectToSerialPort()
        {
            String errorMessage = "";

            try
            {
                if (!ArduinoPort.IsOpen)
                {
                    ArduinoPort.PortName = SerialPortCombo.Text;
                    ArduinoPort.Open();
                    
                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                errorMessage = ex.Message;
            }

            if(ArduinoPort.IsOpen)
            {
                label10.Text = "Connected to Arduino, now sending data...";
                DelfaultSerialPort = ArduinoPort.PortName;
                SaveDefaultPort(DelfaultSerialPort);
            }
            else
            {
                label10.Text = "Disconnected from Arduino: " + errorMessage;
            }
        }
    }
}
