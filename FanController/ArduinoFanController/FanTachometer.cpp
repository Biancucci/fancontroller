// 
// 
// 

#include "FanTachometer.h"


volatile unsigned int FanTachometer::FanRawTickCounter_0 = 0;
volatile unsigned int FanTachometer::FanRawTickCounter_1 = 0;
volatile unsigned int FanTachometer::FanRawTickCounter_2 = 0;
volatile unsigned int FanTachometer::FanRawTickCounter_3 = 0;
volatile unsigned int FanTachometer::FanRawTickCounter_4 = 0;
volatile unsigned int FanTachometer::FanRawTickCounter_5 = 0;
volatile unsigned int FanTachometer::FanRawTickCounter_6 = 0;
volatile unsigned int FanTachometer::FanRawTickCounter_7 = 0;


FanTachometer::FanTachometer(uint8_t fanTachometerPin, unsigned short int interruptIndex)
{
	FanTachometerPin = fanTachometerPin;
	InterruptIndex = interruptIndex;

	pinMode(FanTachometerPin, INPUT);
	LastUpdateTimeMS = millis();

#define INTERRUPT_TYPE FALLING
//#define INTERRUPT_TYPE RISING

	switch (InterruptIndex)
	{
	case 0:
		attachInterrupt(digitalPinToInterrupt(FanTachometerPin), FanTachometer::interrupt_callback_0, INTERRUPT_TYPE);
		FanRawCounterPtr = &FanRawTickCounter_0;
		break;
	case 1:
		attachInterrupt(digitalPinToInterrupt(FanTachometerPin), FanTachometer::interrupt_callback_1, INTERRUPT_TYPE);
		FanRawCounterPtr = &FanRawTickCounter_1;
		break;
	case 2:
		attachInterrupt(digitalPinToInterrupt(FanTachometerPin), FanTachometer::interrupt_callback_2, INTERRUPT_TYPE);
		FanRawCounterPtr = &FanRawTickCounter_2;
		break;
	case 3:
		attachInterrupt(digitalPinToInterrupt(FanTachometerPin), FanTachometer::interrupt_callback_3, INTERRUPT_TYPE);
		FanRawCounterPtr = &FanRawTickCounter_3;
		break;
	case 4:
		attachInterrupt(digitalPinToInterrupt(FanTachometerPin), FanTachometer::interrupt_callback_4, INTERRUPT_TYPE);
		FanRawCounterPtr = &FanRawTickCounter_4;
		break;
	case 5:
		attachInterrupt(digitalPinToInterrupt(FanTachometerPin), FanTachometer::interrupt_callback_5, INTERRUPT_TYPE);
		FanRawCounterPtr = &FanRawTickCounter_5;
		break;
	case 6:
		attachInterrupt(digitalPinToInterrupt(FanTachometerPin), FanTachometer::interrupt_callback_6, INTERRUPT_TYPE);
		FanRawCounterPtr = &FanRawTickCounter_6;
		break;
	case 7:
		attachInterrupt(digitalPinToInterrupt(FanTachometerPin), FanTachometer::interrupt_callback_7, INTERRUPT_TYPE);
		FanRawCounterPtr = &FanRawTickCounter_7;
		break;
	default:
		break;
	}
	
}

int FanTachometer::Tick()
{
	//Disable interrupts
	const unsigned long currentTime = millis();

	volatile int fanCounterLocal = 0;

	noInterrupts();
	//detachInterrupt(digitalPinToInterrupt(FanTachometerPin));
	
	fanCounterLocal = *FanRawCounterPtr;
	*FanRawCounterPtr = 0;

	//Re enable interrupts
	interrupts();

	//Calculate RPM
	#define MillisInAMinute 60000UL
	const unsigned long deltaFromLastUpdate = currentTime - LastUpdateTimeMS;

	int rpm = fanCounterLocal / (((double)deltaFromLastUpdate) / MillisInAMinute);
	rpm /= 2;
	rpm = (rpm >= 0 && rpm <= 4000) ? rpm : 0;

	//Update last time
	LastUpdateTimeMS = currentTime;

	//Serial.println(fanCounterLocal);
	//Serial.println(rpm);

	return rpm;

}


