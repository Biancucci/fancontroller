// FanTachometer.h

#ifndef _FANTACHOMETER_h
#define _FANTACHOMETER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class FanTachometer
{
private:
	//Arduino input pin of the Fan tachometer signal
	uint8_t FanTachometerPin = 0;

	//Last time when the calculation was done, expressed in milliseconds
	unsigned long LastUpdateTimeMS = 0;

	//Last rpm calculation
	int RPM = 0;

	//Interrupt to use
	short int InterruptIndex = -1;

	//Raw counters for each interrupt functions
	static volatile unsigned int FanRawTickCounter_0;
	static volatile unsigned int FanRawTickCounter_1;
	static volatile unsigned int FanRawTickCounter_2;
	static volatile unsigned int FanRawTickCounter_3;
	static volatile unsigned int FanRawTickCounter_4;
	static volatile unsigned int FanRawTickCounter_5;
	static volatile unsigned int FanRawTickCounter_6;
	static volatile unsigned int FanRawTickCounter_7;

	//Local fan counter pointer, assigned in constructor to the right raw counter
	volatile unsigned int* FanRawCounterPtr = 0;

 public:
	 //Be sure that the digital pin can support interrupts, be sure that only this instance will use the same interrupt index
	 FanTachometer(uint8_t fanTachometerPin, unsigned short int interruptIndex);

	 //Calculate the RPM and return it
	 int Tick();

	 //Just return the last calculated value, no calculation is done
	 inline int GetLastRPM();

private:

	static void interrupt_callback_0()
	{
		++FanRawTickCounter_0;
	}

	static void interrupt_callback_1()
	{
		++FanRawTickCounter_1;
	}

	static void interrupt_callback_2()
	{
		++FanRawTickCounter_2;
	}

	static void interrupt_callback_3()
	{
		++FanRawTickCounter_3;
	}

	static void interrupt_callback_4()
	{
		++FanRawTickCounter_4;
	}

	static void interrupt_callback_5()
	{
		++FanRawTickCounter_5;
	}

	static void interrupt_callback_6()
	{
		++FanRawTickCounter_6;
	}

	static void interrupt_callback_7()
	{
		++FanRawTickCounter_7;
	}

};

int FanTachometer::GetLastRPM()
{
	return RPM;
}

#endif

