// 
// 
// 

#include "TermistorReader.h"

TermistorReader::TermistorReader(uint8_t analogInputPin)
{
	AnalogInputPin = analogInputPin;
	pinMode(analogInputPin, INPUT);
}

float TermistorReader::GetTempCelsius(bool& valid_o) const
{
	#define RESITOR_OHM 10000.0

	const int tempReading = analogRead(AnalogInputPin);
	// This is OK
	double tempK = log(RESITOR_OHM * ((1024.0 / tempReading - 1)));
	tempK = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * tempK * tempK)) * tempK);       //  Temp Kelvin
	const float tempC = tempK - 273.15;            // Convert Kelvin to Celsius
	const float tempF = (tempC * 9.0) / 5.0 + 32.0; // Convert Celsius to Fahrenheit
	
	valid_o = tempC >= -20.0;
	return tempC;
}

