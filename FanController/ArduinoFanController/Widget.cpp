// 
// 
// 

#include "Widget.h"

const unsigned int Widget::Width_s = 160;
const unsigned int Widget::LineHeight_s = 26;

void Widget::Setup(DigoleSerialDisp* display, const unsigned int posX, const unsigned int posY, const unsigned int numRow)
{
	Display = display;
	SizeX = 160;
	SizeY = 26* numRow;
	PosX = posX;
	PosY = posY;

	for (unsigned int i = 0; i < MaxLines; ++i)
	{
		Values[i] = 0;
		MinValues[i] = 0;
		MaxValues[i] = 0;
		MeasureUnits[i] = new char[5];
		HeaderWidths[i] = 0;
		*MeasureUnits[i] = NULL;
	}
	for (unsigned int i = 0; i < MaxValuesPerMultipleValueLine; ++i)
	{
		MultipleMeasureUnits[i] = new char[5];
		*MultipleMeasureUnits[i] = NULL;
	}

	Display->setDrawWindow(PosX, PosY, SizeX, SizeY);
	
	//Draw background
	Display->setBgColor(BackgroundColor);
	Display->setColor(BackgroundColor);
	Display->drawBox(0, 0, SizeX, SizeY);
	
	//Set style
	Display->setMode(DRAWMODE_NOBACKGROUND);
	Display->setColor(TextColor);

	//Line 1
	/*Display->setFlashFont(FONT_20);
	Display->setTextPosAbs(2, 24);
	Display->print("Ram");

	//Line 2
	Display->setFlashFont(FONT_20);
	Display->setTextPosAbs(2, 24 + 26);
	Display->print("14.4/16.0");
	Display->setFlashFont(FONT_14);
	Display->print(" GB");
	
	//Line3
	Display->setFlashFont(FONT_20);
	Display->setTextPosAbs(2, 24 + 26 + 26 );
	Display->print("3450");
	Display->setFlashFont(FONT_14);
	Display->print("rpm 100%");*/
}

void Widget::SetStaticLine(const unsigned int rowIndex, const char* text)
{
	//Display->setDrawWindow(PosX, PosY, SizeX, SizeY);
	
	//Display->setMode(DRAWMODE_NOBACKGROUND);
	Display->setFlashFont(FONT_20);
	Display->setTextPosAbs(2, 24 + rowIndex*LineHeight_s);
	Display->print(text);
}

void Widget::SetDataLine(const unsigned int rowIndex, const char* header, unsigned int headerWidth,
	int value, unsigned int minValue, unsigned int maxValue, char* unitMesure)
{
	//Display->setDrawWindow(PosX, PosY, SizeX, SizeY);

	Values[rowIndex] = value;
	MinValues[rowIndex] = minValue;
	MaxValues[rowIndex] = maxValue;
	HeaderWidths[rowIndex] = headerWidth;
	strcpy(MeasureUnits[rowIndex], unitMesure);

	Display->setFlashFont(FONT_20);
	Display->setTextPosAbs(2, 24 + rowIndex*LineHeight_s);
	Display->print(header);
}

void Widget::UpdateDataLine(const unsigned int rowIndex, int value)
{
	Display->setDrawWindow(PosX, PosY, SizeX, SizeY);

	//Display->setMode(DRAWMODE_OVERWRITE);

	Display->setColor(BackgroundColor);
	Display->drawBox(HeaderWidths[rowIndex] + 4, rowIndex*LineHeight_s, Width_s - HeaderWidths[rowIndex], 26);

	Display->setColor(TextColor);
	Display->setFlashFont(FONT_20);
	Display->setTextPosAbs(HeaderWidths[rowIndex] + 4, 24 + rowIndex*LineHeight_s);
	Display->print(value);

	if (*MeasureUnits[rowIndex] != NULL)
	{
		Display->setFlashFont(FONT_14);
		Display->print(MeasureUnits[rowIndex]);
	}

	if (MinValues[rowIndex] != MaxValues[rowIndex])
	{
		Display->print(" ");
		Display->print(map(value, MinValues[rowIndex], MaxValues[rowIndex], 0, 100));
		Display->print("%");
	}

	//Display->resetDrawWindow();
}

void Widget::UpdateDataLine(const unsigned int rowIndex, char* value)
{
	Display->setDrawWindow(PosX, PosY, SizeX, SizeY);

	//Display->setMode(DRAWMODE_OVERWRITE);

	Display->setColor(BackgroundColor);
	Display->drawBox(HeaderWidths[rowIndex] + 4, rowIndex*LineHeight_s, Width_s - HeaderWidths[rowIndex], 26);

	Display->setColor(TextColor);
	Display->setFlashFont(FONT_20);
	Display->setTextPosAbs(HeaderWidths[rowIndex] + 4, 24 + rowIndex*LineHeight_s);
	Display->print(value);
}

void Widget::SetMultipleValueDataLine(const unsigned int rowIndex, char* measureUnit1, char* measureUnit2, char* measureUnit3)
{
	strcpy(MultipleMeasureUnits[0], measureUnit1);
	strcpy(MultipleMeasureUnits[1], measureUnit2);
	strcpy(MultipleMeasureUnits[2], measureUnit3);
}

