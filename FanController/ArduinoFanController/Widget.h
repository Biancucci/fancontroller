// Widget.h

#ifndef _WIDGET_h
#define _WIDGET_h

#define _Digole_Serial_UART_
#include "Colors.h"
#include <DigoleSerial.h>

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class Widget
{
public:
	static const unsigned int Width_s;
	static const unsigned int LineHeight_s;

private:
	#define FONT_20 600000
	#define FONT_17 700000
	#define FONT_14 500000

	#define DRAWMODE_NOBACKGROUND '0'
	#define DRAWMODE_OVERWRITE 'C'

	DigoleSerialDisp* Display = NULL;
	unsigned int SizeX = 0;
	unsigned int SizeY = 0;
	unsigned int PosX = 0;
	unsigned int PosY = 0;

	const uint8_t BackgroundColor = 142;
	const uint8_t TextColor = WHITE_8bit;

	#define MaxLines 10

	int Values[MaxLines];
	int MinValues[MaxLines];
	int MaxValues[MaxLines];

	unsigned int HeaderWidths[MaxLines];

	char* MeasureUnits[MaxLines];

	//Support only one multiple value rows
	#define MaxValuesPerMultipleValueLine 3
	char* MultipleMeasureUnits[MaxValuesPerMultipleValueLine];

	//const unsigned int sizeX, 
	//const unsigned int sizeY;

 public:
	 void Setup(DigoleSerialDisp* display, const unsigned int posX, const unsigned int posY,
		 const unsigned int numRow);

	 void SetStaticLine(const unsigned int rowIndex, const char* text);

	 void SetDataLine(const unsigned int rowIndex, const char* header, unsigned int headerWidth,
		 int value, unsigned int minValue, unsigned int maxValue, char* unitMesure);

	 void UpdateDataLine(const unsigned int rowIndex, int value);
	 void UpdateDataLine(const unsigned int rowIndex, char* value);

	 //Only one of this type is supported per each widget
	 void SetMultipleValueDataLine(const unsigned int rowIndex, char* measureUnit1,	char* measureUnit2, char* measureUnit3);

	 template<typename ValueType1, typename ValueType2, typename ValueType3 >
	 void UpdateMultipleValueDataLine(const unsigned int rowIndex, ValueType1 value1, ValueType2 value2, ValueType3 value3);
};

template<typename ValueType1, typename ValueType2, typename ValueType3 >
void Widget::UpdateMultipleValueDataLine(const unsigned int rowIndex, ValueType1 value1, ValueType2 value2, ValueType3 value3)
{
	Display->setDrawWindow(PosX, PosY, SizeX, SizeY);

	//Display->setMode(DRAWMODE_OVERWRITE);
	Display->setColor(BackgroundColor);
	Display->drawBox(2, rowIndex*LineHeight_s, Width_s, 26);
	
	Display->setColor(TextColor);
	Display->setTextPosAbs(2, 24 + rowIndex*LineHeight_s);

	Display->setFlashFont(FONT_20);
	Display->print(value1);
	Display->setFlashFont(FONT_14);
	//if (*MeasureUnits[0] != NULL)
	{
		Display->print(MultipleMeasureUnits[0]);
	}
	Display->print(" ");

	Display->setFlashFont(FONT_20);
	Display->print(value2);
	Display->setFlashFont(FONT_14);
	//if (*MeasureUnits[1] != NULL)
	{
		Display->print(MultipleMeasureUnits[1]);
	}
	Display->print(" ");

	Display->setFlashFont(FONT_20);
	Display->print(value3);
	//if (*MeasureUnits[2] != NULL)
	{
		Display->setFlashFont(FONT_14);
		Display->print(MultipleMeasureUnits[2]);
	}
}

#endif

