// TermistorReader.h

#ifndef _TERMISTORREADER_h
#define _TERMISTORREADER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class TermistorReader
{
private:
	//Arduino input pin of the Fan tachometer signal
	uint8_t AnalogInputPin = 0;

 public:
	TermistorReader(uint8_t analogInputPin);

	float GetTempCelsius(bool& valid_o) const;
};

#endif

